const moveDots = (x, y, elementArray) => {
  elementArray.forEach((item, i) => {
    setTimeout(() => {
      item.style.left = `${x - 20 + i * 5}px`;
      item.style.top = `${y - 20 + i * 5}px`;
    }, i * 100);
  });
};

const handleMouseMove = (e, main1, main2, main3) => {
  const x = e.clientX;
  const y = e.clientY;
  moveDots(x, y, [main1, main2, main3]);
};

window.addEventListener("load", () => {
  const main1 = document.getElementById("main1");
  const main2 = document.getElementById("main2");
  const main3 = document.getElementById("main3");
  document.addEventListener("mousemove", (e) =>
    handleMouseMove(e, main1, main2, main3)
  );
});
